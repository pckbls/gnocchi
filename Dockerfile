##
## Copyright (c) 2021 The Yaook Authors.
##
## This file is part of Yaook.
## See https://yaook.cloud for further info.
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##
FROM almalinux:8

ARG branch=stable/queens
ARG gnocchi_branch=stable/4.4

COPY files/ceph.repo /etc/yum.repos.d/ceph.repo

RUN yum install -y epel-release 

RUN yum install -y python3 python-rados python-rbd pkgconf-pkg-config python3-devel git gcc

RUN git clone https://github.com/openstack/requirements.git --depth 1 --branch ${branch} ;
RUN git clone https://github.com/gnocchixyz/gnocchi --depth 1 --branch ${gnocchi_branch} ;
RUN pip3 install setuptools==45 ; `# WORKAROUND for https://github.com/pallets/markupsafe/issues/116`
RUN pip3 install wheel;
RUN pip3 install -c requirements/upper-constraints.txt PyMySQL python-memcached pymemcache gnocchiclient gnocchi[keystone] sqlalchemy_utils oslo_db==10.0.0 uwsgi; `# olso_db must be <11 as that would depend on SQLAlchemy 1.4 which is not allowed by upper-constraints`
RUN pip3 install --upgrade boto3 "urllib3<1.23,>=1.21.1"; `# Needs to be a separate step to upgrade required dependencies. But we need to keep to a specific urllib version range, the limit is taken 1:1 from upper-constraints.txt`
RUN rm -rf requirements gnocchi;
